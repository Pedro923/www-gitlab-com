---
layout: handbook-page-toc
title: "GitLab swag program"
description: "This page outlines ways to request swag."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Swag shop

Check out the [**GitLab Swag Shop**](https://www.shop.gitlab.com)! 

Free shipping is available to GitLab team members for all items in the swag shop. Visit the #swag Slack channel for the code.
     
**_Don't see an item you're looking for?_** Submit requests for future items you'd like to see in the swag shop in [this issue](https://gitlab.com/gitlab-com/marketing/brand-product-marketing/brand-strategy/-/issues/11).

_**Note:**_ Please do not work with other vendors outside of our approval process to print your own swag. It's important that we all protect our brand integrity and avoid creating items that violate [brand standards](https://design.gitlab.com/).

### Ordering swag for customers

If you want to order swag for customers, your team, events, giveaways, TMRGs, etc., please ensure your team has budget to cover the items, as well as a purchase order (PO) [in Coupa](https://about.gitlab.com/handbook/finance/procurement/coupa-faq/) to cover the order. Unsure if you have budget? Please reach out to your manager or Finance Business Partner. The Brand team does not have budget to cover other departments' swag needs.

**How to make a request:**

1. Be sure you have budget for the order and either approval to expense the swag or a PO in Coupa. If you're unsure, confirm with your manager. The Brand team does not have budget to cover other teams' swag needs.
1. Email Savanah at `ssporer@boundlessnetwork.com`. Include the quantity, total budget (must include shipping, tax, and customs fees), and ideas of items or brands (e.g. jacket, sweater, mug). Please allow at least 4 weeks lead time. If your request is more urgent, ask Savanah if an expedited shipping option is available (include a business reason for the rush).
1. You will be set up with a Boundless account to see ideas and suggestions.
1. Savanah will send you a spreadsheet template to fill out with addresses. The information must be added in the provided format.
1. Once you share the PO with Savanah, the Boundless team will upload the invoice to streamline billing.

### Giveaways and store credit

#### Organizing a giveaway or gift

If you'd like to allow giveaway (e.g. Hackathon winners) or gift recipients to choose their own swag, we can set up a store credit for them to use in the GitLab swag shop.

1. Email Jeremy from the Boundless team at `jnewman@boundlessnetwork.com`. 
1. Include first and last name of recipient, email address, and department to be charged for the store order. 
1. Jeremy will respond with instructions on how each person can access the credit. 

Note: This is only for items currently on the retail store. You will only be charged for the amount redeemed in the store once it is spent.  

#### Gift of choice landing page

If you’d like to create a unique giveaway for a group of more than 15 people with items that are not on the swag store, a “gift of choice" redemption landing page can be set up. This is a great option for a special group who attended an event or won a prize. Recipients enter their own information and the gift is shipped directly to them. 

_Note: You'll need to have approved budget before requesting the page. If you're not sure about budget, please check with your manager._ 

**How to submit the request:**

1. Email Savanah at `ssporer@boundlessnetwork.com`. Include: approved budget, items to consider, approximately how many people will be offered the gift. 
1. Once items are finalized and ordered, a landing page will be built with an image of the items, drop downs to select size, and contact fields to capture recipient shipping addresses. 
1. After the addresses are collected, the kits will be filled and shipped out to each person. 
1. You will be invoiced for the items and the drop shipping for each person. 


### Swag for TMRGs

There are two main giveaway options for Team Member Resource Groups (TMRGs): A one-time send, or a continuous giveaway and storage plan.

**One-time giveaways:**
- Are cheaper as there are no storage fees.
- Can support both internal and external giveaways.
- Must have approved budget.

**Continuous giveaways and long-term storage option:**
- Best for groups wanting to send items to group members as they join the group.
- Must have a pre-approved budget to purchase the inventory of items. Items are invoiced at the time of ordering. 
- Purchased items will be sent to the inventory warehouse and listed on an internal swag store for on-demand fulfillment. 
- Your department is billed monthly for shipping fees (to each event or person), as well as storage fees for your inventory.
- You will work with Savanah (`ssporer@boundlessnetwork.com`) to select the items to be purchased and ongoing inventory management of your items. 

### Swag for GitLab-owned or sponsored events

[See more details on the Events page.](/handbook/marketing/events/#swag-for-events)

#### Requesting guidelines

At least 3 weeks before your event, email `ssporer@boundlessnetwork.com` for any new swag requests. In the request, include: 

1. Approved budget
1. Date of event or date swag is needed
1. Quantity of products you’ll need to purchase
1. Ideas of items you’d like to consider

We have a list of approved items you can browse. Any new items must be approved by the Brand team for brand consistency. The Boundless team will work with the GitLab Brand team for approval of final designs.

**Paper and print collateral:** To be most efficient, we do not make custom print assets for events. Printed materials become instantly out of date, and we want to help reduce waste.

**Tablecloths and banners:** To order tablecloths or banners for events, please reach out to Savanah at `ssporer@boundlessnetwork.com`.

### Stickers

For larger sticker orders (stickers in a quantity of 100 or greater), do not go through the swag store. Instead, use our [Stickermule](https://www.stickermule.com/) account or contact `ssporer@boundlessnetwork.com`. Include the address, date needed, and order quantity.

### Ordering on StickerMule

StickerMule is the preferred vendor for orders that include only stickers, or orders that include a large number of stickers.

**Examples of events where you might order stickers in large quantities:**
* Hackathons
* Conferences
* MeetUps

**How to order:** 
1. Log in to StickerMule
1. Navigate to the 'Reorder' tab
1. Select the sticker you'd like to order based on stickers created from previous orders. New stickers can be created, but if possible, it is faster to reorder an existing sticker.
1. Existing  stickers vary in size, but most are between 2.64"x3"
1. Stickers must be orders in batches of 10
1. Enter sticker quantity and process Stickermule order using the corporate marketing GitLab card.
1. Send orders to the wider community member, Meetup organizer, customer.
1. When adding shipping information, add the project title or finance tag to the `Company` line so that orders can be tracked.

### Delayed or lost orders

From time to time, a package gets lost, delayed, or stuck in customs. If the order was placed on the swag store, you can log in to your account to access tracking. 

If this was a special order, or you need assistance with tracking, reach out to Savanah at `ssporer@boundlessnetwork.com` or in the #swag channel.

### Suggesting new items or designs

Have feedback or suggestions for items you'd like to see in the swag shop? Please add them to [this issue](https://gitlab.com/gitlab-com/marketing/brand-product-marketing/brand-strategy/-/issues/11). The Brand team is growing and more iterations will be made to this process and program in FY24.

## DRIs for the swag program

| Topic | DRI |
| ------ | ------ |
|    Swag shop and special orders    |    Savanah at Boundless: `ssporer@boundlessnetwork.com`    |
|    Swag program strategy, vendor relationships    |    Brand Strategy team: `@bbula`    |
|    Swag design    |    Brand Creative team: `@amittner`    |
|    New hire swag    |    Talent Brand team: `@drogozinski`    |
|   Anniversary swag     |    People Connect team: `@ameeks`    |

If you have other questions, please send a message in the #swag Slack channel. 
