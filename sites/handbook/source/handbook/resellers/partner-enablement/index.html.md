---
layout: handbook-page-toc
title: "Channel Partner Field Team Enablement"
---


## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}
<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />
{::options parse_block_html="true" /}

# GitLab’s differentiation in the ecosystem
{: .gitlab-orange}

GitLab is a public company that provides commercial support and sells per-user subscription licenses of GitLab - The DevSecOps Platform - as a product to enterprises globally. GitLab simplifies DevOps chains of custom integrated tools in the ecosystem into a single application or platform. GitLab Enterprise Edition, based on the core open-source GitLab Community Edition, provides commercial support, training, and professional service offerings. GitLab’s core codebase is backed by millions of users in the community, with incredible innovation due to the contributions of thousands of developers globally.

GitLab, as a DevSecOps platform, is better experienced than described. When customers adopt GitLab, they typically do so to solve a particular DevOps problem. Successful customers quickly realize that GitLab’s complete set of software planning, collaborating, revision controlling, security verifying, deploying, and monitoring is unlike any other DevOps software tool available today. This platform-based approach is a crucial differentiator among other git-based revision control systems that rely on a network of ‘plugins’ and ‘add-ons’ to complete a fully functional software delivery factory. GitLab as a platform encompasses the entire “toolchain” of ecosystem tools, an improvement from cobbled-together toolchains.

Long-lived customers adopt new features and experience the power of pre-integrated features that work well together. With GitLab, pipelines become easier, DevOps pros begin to solve higher-order problems, and you sell more.

# Here’s how you make money with GitLab
{: .gitlab-orange}
The three main areas of making money with GitLab are selling Licensing, Training, and Professional (Consultative) Services.

**$  License Revenue:**  Lead with full [GitLab Ultimate](https://about.gitlab.com/pricing/ultimate/), and scale back to [GitLab Premium](https://about.gitlab.com/pricing/premium/) for narrower use cases or price-adverse buyers. [Register the deal on partners.gitlab.com](https://about.gitlab.com/handbook/resellers/channel-working-with-GitLab/#submitting-a-deal-registration), and take down the initial sale of GitLab Licenses.

**$$ Training and Professional Services (PS):**  Customers new to GitLab will likely need help understanding the more effecient GitLab Flow Workflow. Our [GitLab Flow Best Practices](https://about.gitlab.com/topics/version-control/what-are-gitlab-flow-best-practices/) is a great place to start learning about how to work more efficiently. Also see the bottom of the page for [videos about GitLab Flow.](https://about.gitlab.com/handbook/resellers/partner-enablement/#third-party-tutorials)

New customers to GitLab will need help migrating from existing tools and Git-based software sytems. You are welcome to resell GitLab's Professioanl Service offerings - or, as a partner, you can offer you own similar migration, training and enablement offerings. [Conduct the Project Service](https://partners.gitlab.com/prm/English/c/Services) of installation, configuration and integration, and migration from other tools.

**$$$  License Growth Revenue:**  We have a Land and Expand model.  After successful customers adopt GitLab to solve a single-point problem, many discover that GitLab can replace and remove duplicative software tools.  When using GitLab as an end-to-end software development and delivery platform, removing redundant software tools becomes evident.  We focus our [incentives for partners](https://partners.gitlab.com/prm/English/s/assets?id=350001) on new licenses (Land) and license growth over the baseline (Expand).  We focus on the increased number of GitLab licensed users because we provide commercial, technical support, so you don’t have to (as much).  Partners play a crucial role in practice and integration support.  GitLab maintains a commercial support team that other DevOps product toolchain companies cannot provide as part of our single product platform viewpoint.  Customers buy more GitLab from you by vertically expanding their footprint from Premium to Ultimate licenses or increasing their user count horizontally.

**$$$ Transformation Services:**  Our partners find that GitLab breath and depth provide a long tail of strategic growth in customer consulting.  Customers improve their DevSecOps practices by leveraging GitLab’s workflow.  Treat the platform as an ongoing reason for client engagement.  In starting with GitLab, ensure all project managers, developers, security engineers, and managers can log in and access the platform.  Use the shareable links anywhere within GitLab to allow the different roles to know where their planning, development, security reports, and value stream reporting are.  Next, enable security scanning and reporting.  Next, allow auto-deployment to the cloud.  Because the breadth and depth of GitLab’s software factory-like platform are so broad and deep, coaching successful customers towards DevOps best practices becomes a natural opportunity for you to sell additional services.  For a large enterprise, use GitLab to provide a roadmap of where you will take your customer with GitLab in their software development maturity journey.  Coaching and consultative services will also offer a roadmap for revenue quarter after quarter.


## Ready to get Started?
{: .gitlab-purple}
[Click Here](https://partners.gitlab.com/English/register_email.aspx) to apply to GitLab's Partner Program.

# Learning More
{: .gitlab-orange}
You will find sales guides, use cases, training materials, and program guides in the material below.  The materials should be a great place to start effectively selling, serving and hitting your number with GitLab.

Some of the links below require you to login to [GitLab's Partner Portal](https://partners.gitlab.com/) first.

## Partner Program 
{: .gitlab-purple}

-   **Partner Program Overview:** [FY23 GitLab Channel Program Guide](https://partners.gitlab.com/prm/English/s/assets?id=414926) This document includes details about sales tracks, benefits, and requirements at different partner levels.  (Be sure to log in to partners.gitlab.com first to access this link.)

-   **How to request Not For Resale (NFR) licenses:**  Each GitLab partner is allowed one set of annual NFR licenses for a GitLab Self Managed Instance and a GitLab.com SaaS license [per this handbook page](https://about.gitlab.com/handbook/sales/channel/channel-programs-ops/#internal-nfr-request-processes).   [Partner Portal](https://partners.gitlab.com/) > [Services](https://partners.gitlab.com/prm/English/c/Services) > [Request an NFR](https://docs.google.com/forms/d/e/1FAIpQLSc8jdwNrVMfVQ4x7XWXwcaMrcO4m2C6nGEq3_tKQWJnftAN_g/viewform)

-   **Training and Certifications:** Available on the Training > Start Training tab at the top of the [GitLab Partner Portal](https://partners.gitlab.com/) or by accessing [https://levelup.gitlab.com/](https://levelup.gitlab.com/) after authenticating to the GitLab Partner Portal.

-   **GitLab Certification Guide:**  You can share this [GitLab Certification Guide](https://content.gitlab.com/viewer/639a5b13fc695b0f033c4ff9) with your teams describing the GitLab Certifications
-   **GitLab Training:**  This [Handbook page](https://about.gitlab.com/handbook/resellers/training/) on training available for partners
-   **Engaging Partner Support:** [Handbook Page](https://about.gitlab.com/handbook/resellers/partner-support/)
-   **GitLab Partner Locator:** [Partner Catalog](https://about.gitlab.com/partners/)

# Basic Presales Knowledge
{: .gitlab-orange}

## Get to know GitLab, what it is, what it offers, and what it does
{: .gitlab-purple}

1.  GitLab [promo video](https://player.vimeo.com/video/702922416) on [https://about.gitlab.com](https://about.gitlab.com) (1 min)

2.  GitLab [promo demo video](https://about.gitlab.com/demo/) (3 min)

3.  Get to know the general capability set at [https://about.gitlab.com/platform](https://about.gitlab.com/platform) (be sure to scroll down to the GitLab Features section and then click on the stages of the DevSecOps process (Plan, Create, Verify, Package, etc.) for more information

4.  Our [Solutions page](https://about.gitlab.com/solutions/) offers a wide variety of ways customers leverage GitLab


## Pitch GitLab and uncover new opportunities
{: .gitlab-purple}

1.  Please register for the [Building Pipelines GitLab Partner Webinar Series,](https://content.gitlab.com/viewer/63bddf02edadd0b1346a73db) where we discuss various sales and presales-level topics that help you build your sales pipeline with GitLab.

2.  Review [Positioning GitLab - Handbook Page](https://about.gitlab.com/handbook/positioning-faq/)

3.  Discovery is a crucial skill for any Solutions Architect (SA).  [Here is how GitLab SA’s do their Discovery](https://about.gitlab.com/handbook/sales/qualification-questions/#questions-for-selling-gitlab-premium-and-ultimate).

4.  Our [Customer Case Studies](https://about.gitlab.com/customers/) will give you ideas of why enterprises are choosing GitLab.  Bookmark this for customer justification in deals later.

5.  There are different [User Personas](https://about.gitlab.com/handbook/product/personas/) vs. [Buyer Personas](https://about.gitlab.com/handbook/marketing/brand-and-product-marketing/product-and-solution-marketing/roles-personas/buyer-persona/) to understand when selling a Platform like GitLab.  Which do you value more?


## Learn how to solution and transact a GitLab deal
{: .gitlab-purple}

1.  GitLab has a leading market share in self-managed deployments.  Here are our [reference architectures](https://docs.gitlab.com/ee/administration/reference_architectures/) and [install guides](https://docs.gitlab.com/ee/install/).  

2.  SaaS on GitLab.com is an increasingly popular option.  [Start a Free Trial](https://gitlab.com/-/trials/new) now.

3.  Pricing is a simple per-user subscription that is the same whether you are using SaaS or Self Hosted instance.  Customers see the [List Price here](https://about.gitlab.com/pricing).  Review the [Partner Program Incentives Guide](https://partners.gitlab.com/prm/English/s/assets?id=350001) for more details on partner transactions.

4.  There are [online ROI calculators;](https://about.gitlab.com/calculator/), your partner team can share more advanced ROI tools.


## Services opportunities when selling Gitlab
{: .gitlab-purple}

1.  Here are some [service kits](https://partners.gitlab.com/prm/English/c/Channel_Service_Packages) we have developed, including sample Statements of Work (SOWs) and Levl of Effort (LOEs).

2.  These are the GitLab PS [Delivery Kits](https://gitlab.com/gitlab-org/professional-services-automation/delivery-kits) our teams use for customer projects.


# Advanced Presales & Technical Knowledge
{: .gitlab-orange}

## Compete
{: .gitlab-purple}

* [GitLab Maturity Page](https://about.gitlab.com/direction/maturity/): A very transparent assessment of the maturity of each GitLab feature. No direct comparisons with other vendors here.

* [GitLab Customer Success Stories](https://about.gitlab.com/customers/): Our success stories, including customer tesimonials.

* [DevOps Tools Landscape](https://about.gitlab.com/devops-tools/): A great place to learn about how GitLab compares to its competitors. A more detailed view of the same is the [DevOps maturity comparison chart](https://about.gitlab.com/competition/). Clicking on some product names (on both pages) takes the viewer to a detailed comparison of GitLab and the selected competitor. For example: [GitLab vs. GitHub](https://about.gitlab.com/competition/github/).

## Technical
{: .gitlab-purple}

* [GitLab Documentation](https://docs.gitlab.com/): The best starting point for technical questions. It lists feature descriptions, excellent tutorials, and troubleshooting tips.

* [GitLab Blog](https://about.gitlab.com/blog/): Various content about GitLab. Mostly technical stuff, but not exclusively. Subscribing to the newsletter can be useful.

* [Release Overview Website](https://gitlab-com.gitlab.io/cs-tools/gitlab-cs-tools/what-is-new-since/?tab=features): A fairly unknown tool that can help with understanding the changes in each GitLab version. (Source code of the website is [here](https://gitlab.com/gitlab-com/cs-tools/gitlab-cs-tools/what-is-new-since).) Important features of this website to pay extra attention to:

    * [CVEs by version](https://gitlab-com.gitlab.io/cs-tools/gitlab-cs-tools/what-is-new-since/?tab=cves): easily keep track of security vulnerabilities in each GitLab version.

    * [Upgrade path](https://gitlab-com.gitlab.io/support/toolbox/upgrade-path/): a handy tool which can list the steps of upgrading self-managed GitLab from version X to version Y.

* [Deprecations by version](https://docs.gitlab.com/ee/update/deprecations.html): same data as [this page](https://gitlab-com.gitlab.io/cs-tools/gitlab-cs-tools/what-is-new-since/?tab=deprecations) of the Release Overview website, just in a different format.

* [GitLab Releases Blog](https://about.gitlab.com/releases/categories/releases/): The same information as the Release Overview website, but in nicely formatted blog posts, ready to share with customers.

* [GitLab's Repository](https://gitlab.com/gitlab-org/gitlab): The source code of the GitLab software itself. The most useful part is the [Issues page](https://gitlab.com/gitlab-org/gitlab/-/issues), where everybody can check the state of a feature or a bug and see if it's planned or being worked on.


# Running Effective GitLab Demos and POCs
{: .gitlab-orange}

## Demo resources to get started showcasing the Platform
{: .gitlab-purple}

1.  The GitLab [Demo Handbook Page.](https://about.gitlab.com/handbook/customer-success/solutions-architects/demonstrations/)

2.  Check out the presentation and video in [Building Pipelines Partner Webinar](https://content.gitlab.com/viewer/63bddf02edadd0b1346a73db) Episode 011 on “Effective GitLab Demos.” (40:16)  More resources and helpful hints are provided.

3.  [GitLab Product Marketing](https://about.gitlab.com/handbook/marketing/brand-and-product-marketing/product-and-solution-marketing/#key-demo-videos) has some product demo videos on their handbook page.

4. [GitLab Partner Demo Delivery Guide](https://gitlab.com/gitlab-partner-demos/delivery-guide): a repository containing all the resources for partners to successfully deliver a 1-hour-long, high-level, technical overview demo of GitLab.


## Existing demo projects and guides you can leverage
{: .gitlab-purple}

1.  [Ultimate GitOps Workshop](https://guided-explorations.gitlab.io/workshops/gitlab-for-eks/): Leverage GitLab K8s agent to deploy to EKS.

1.  GitLab initiative to create demos you can leverage: [GitLab Learn Labs.](https://gitlab.com/gitlab-learn-labs/webinars/how-to-use-these-projects)

1.  Sometimes the demo script is [found in an issue](https://gitlab.com/gitlab-learn-labs/webinars/workshop-project/-/issues/1), not the Readme.  Not every demo project has a script (sorry).

1.  [Guided Explorations](https://gitlab.com/guided-explorations):  Contains Joint Reference Architectures, Patterns, and Working Examples for integrating GitLab with Alliance and Technology solutions.

1.  [Customer Success Workshops](https://gitlab.com/gitlab-com/customer-success/workshops/templates):  A working group containing all CS Workshop Templates.

1.  [SA Demo Catalog](https://gitlab.com/gitlab-com/customer-success/solutions-architecture/demo-catalog):  Demo Archive of SA Demo's which can be reused or repurposed for your demos.


## Proof of Value (POV), aka POC’s
{: .gitlab-purple}

Here’s the [handbook page](https://about.gitlab.com/handbook/customer-success/solutions-architects/tools-and-resources/pov/) for conducting POVs


# Additional Resources
{: .gitlab-orange}

## Third-Party Tutorials
{: .gitlab-purple}

1.  **GitLab for Beginners:**
    - [Learn GitLab in 3 Hours GitLab Complete Tutorial For Beginners](https://www.youtube.com/watch?v=8aV5AxJrHDg) (~ 3 hrs)
1.  **GitLab Flow:**
    - [GitLab Flow - GitLab Tutorial - Part I](https://www.youtube.com/watch?v=ZJuUz5jWb44) (15:37)
    - [GitLab Flow - GitLab Tutorial - Part II](https://www.youtube.com/watch?v=K5Ux_m0ccuo&list=RDCMUCxu8xrbPZ-neg3FhMdz4yrA&index=3) (33:00)
1.  **GitLab CI/CD:**
    - [GitLab CI/CD - GitLab Runner Introduction - 2022](https://www.youtube.com/watch?v=-CyVpfDQAG0&list=RDCMUCxu8xrbPZ-neg3FhMdz4yrA&index=6)(23:27)
    - [GitLab CI CD - Install and Configure GitLab Runner on Kubernetes with Helm](https://www.youtube.com/watch?v=0Fes86qtBSc) (29:41)
1.  **GitLab Unfiltered:**
    - GitLab Security Capabilities
[A tour of GitLab Security capabilities](https://www.youtube.com/watch?v=d8X6vYY4WOA) (50:16)
1. **Other Good Assets:**
    - [Partner Enablement Presentation](https://docs.google.com/presentation/d/1VUY6WR4gewnXITUUAnVbFOExXmhdTYif2JgAiTbT7tE/edit#slide=id.g12b319f6181_0_10)
